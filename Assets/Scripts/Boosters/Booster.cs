using UnityEngine;

public class Booster : Clickable
{
    [SerializeField] private protected float lifetime;
    [SerializeField] private protected GameObject idleEffect;

    protected void OnUpdate()
    {
        var transform1 = transform;
        var rotation = transform1.rotation;
        transform.Rotate(0, 0.5f, 0);
        
        if (lifetime > 0) lifetime -= Time.deltaTime;
        else Destroy(gameObject);

        Instantiate(idleEffect, transform.position, Quaternion.identity);
        
        UpdateClickCheck();
    }
}
