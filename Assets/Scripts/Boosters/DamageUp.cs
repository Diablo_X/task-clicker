using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageUp : Booster
{
    private GameController _controller;
    
    private void Start()
    {
        Camera = Camera.main;
        _controller = FindObjectOfType<GameController>();
    }

    private void Update()
    {
        OnUpdate();
    }

    protected override void OnClick()
    {
        _controller.audioSource.PlayOneShot(onClickSound);
        _controller.damageUpTime += 5;
        
        Destroy(gameObject);
    }
}
