using UnityEngine;

public class EnemyKills : Booster
{
    private GameController _controller;
    
    private void Start()
    {
        _controller = FindObjectOfType<GameController>();
        Camera = Camera.main;
    }

    private void Update()
    {
        OnUpdate();
    }

    // ReSharper disable Unity.PerformanceAnalysis
    protected override void OnClick()
    {
        _controller.audioSource.PlayOneShot(onClickSound);
        
        foreach (var enemy in FindObjectsOfType<Enemy>())
        {
            Destroy(enemy.gameObject);
        }
        
        Destroy(gameObject);
    }
}
