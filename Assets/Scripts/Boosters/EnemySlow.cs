using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySlow : Booster
{
    private GameController _controller;
    
    private void Start()
    {
        Camera = Camera.main;
        _controller = FindObjectOfType<GameController>();
    }

    private void Update()
    {
        OnUpdate();
    }

    protected override void OnClick()
    {
        _controller.audioSource.PlayOneShot(onClickSound);
        
        foreach (var enemy in FindObjectsOfType<Enemy>())
        {
            enemy.GetComponent<Enemy>().freezeTime += 3;
        }
        
        _controller.enemySlowTime += 3;
        Destroy(gameObject);
    }
}
