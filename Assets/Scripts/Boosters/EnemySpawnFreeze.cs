using UnityEngine;

public class EnemySpawnFreeze : Booster
{
    private GameController _controller;
    
    private void Start()
    {
        Camera = Camera.main;
        _controller = FindObjectOfType<GameController>();
    }

    private void Update()
    {
        OnUpdate();
    }

    protected override void OnClick()
    {
        _controller.audioSource.PlayOneShot(onClickSound);
        _controller.freezeTime += 3;
        Destroy(gameObject);
    }
}
