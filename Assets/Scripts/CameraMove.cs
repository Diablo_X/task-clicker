using UnityEngine;

public class CameraMove : MonoBehaviour
{
    [SerializeField] private Vector2 xBorders;
    [SerializeField] private Vector2 yBorders;

    private void Update()
    {
        CameraDrag();
    }

    private void CameraDrag()
    {
        if (Input.touchCount <= 0 || Input.GetTouch(0).phase != TouchPhase.Moved) return;
        var touchDelta = Input.GetTouch(0).deltaPosition;
        Transform transform1;
        (transform1 = transform).Translate(-touchDelta.x * 0.01f, 0, -touchDelta.y * 0.01f);

        var position = transform1.position;
        position = new Vector3(
            Mathf.Clamp(position.x, xBorders.x, xBorders.y),
            position.y,
            Mathf.Clamp(position.z, yBorders.x, yBorders.y));
        transform.position = position;
    }
}
