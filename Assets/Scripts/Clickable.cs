using UnityEngine;

public abstract class Clickable : MonoBehaviour
{
    private protected Camera Camera;
    
    private Ray _ray;
    private RaycastHit _hit;
    [SerializeField] private protected AudioClip onClickSound;
    
    protected virtual void OnClick() { }
    
    // ReSharper disable Unity.PerformanceAnalysis
    protected void UpdateClickCheck()
    {
        if (!Input.GetMouseButtonDown(0)) return;
        _ray = Camera.ScreenPointToRay(Input.mousePosition);
        if (!Physics.Raycast(_ray, out _hit, 1000f)) return;
        if (_hit.transform != transform) return;
        
        OnClick();
    }
}
