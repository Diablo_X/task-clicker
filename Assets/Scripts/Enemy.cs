using UnityEngine;
using Random = UnityEngine.Random;

public class Enemy : Clickable
{
    [SerializeField] private GameObject spawnEffect;
    [SerializeField] private GameObject deathEffect;
    [SerializeField] private GameObject getDamageEffect;

    [SerializeField] private int hp;
    [SerializeField] private float speed;
    [SerializeField] private AudioClip deathSound;
    
    [HideInInspector] public float freezeTime;
    
    private Vector3 _destination;
    private GameController _controller;
    private Animator _animator;
    
    private void Start()
    {
        _animator = GetComponent<Animator>();
        _animator.speed = 0.7f + speed / 2;
        Instantiate(spawnEffect, transform.position, Quaternion.identity);
        
        Camera = Camera.main;
        _controller = FindObjectOfType<GameController>();
        freezeTime = _controller.freezeTime;
        
        MoveDestinationGenerate();
    }

    private void Update()
    {
        if (freezeTime > 0) freezeTime -= Time.deltaTime;
        
        if (_controller.gameStopped) return;
        UpdateClickCheck();
    }

    protected override void OnClick()
    {
        hp -= _controller.currentDamage;
        Instantiate(getDamageEffect, transform.position, Quaternion.identity);

        if (hp > 0)
        {
            _controller.audioSource.PlayOneShot(onClickSound);
            return;
        }
        
        Instantiate(deathEffect, transform.position, Quaternion.identity);
        _controller.audioSource.PlayOneShot(deathSound);
        Destroy(gameObject);
    }

    public void OnDestroy()
    {
        _controller.enemyCount--;
    }
    
    private void FixedUpdate()
    {
        if (_controller.gameStopped) return;
        
        if (freezeTime <= 0)
        {
            if (_animator.speed == 0) _animator.speed = 0.7f + speed / 2;
            MoveToDestination();
        }
        else if (_animator.speed != 0) _animator.speed = 0;
    }

    private void MoveToDestination()
    {
        transform.position = Vector3.MoveTowards(transform.position, _destination, speed * Time.deltaTime);
        MoveDestinationCheck();
    }

    private void MoveDestinationCheck()
    {
        if (Mathf.Abs(transform.position.x - _destination.x) < 1 && Mathf.Abs(transform.position.z - _destination.z) < 1) MoveDestinationGenerate();
    }
    
    private void MoveDestinationGenerate()
    {
        _destination = new Vector3((float)Random.Range(-600, 600) / 100, -6.2f, (float)Random.Range(-200, 1600) / 100);
        transform.rotation = Quaternion.LookRotation(_destination - transform.position);
    }
}
