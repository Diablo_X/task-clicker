using System;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class GameController : MonoBehaviour
{
    [SerializeField] private AudioClip objectSpawnSound;
    [SerializeField] private AudioClip buttonClickSound;
    [SerializeField] private AudioClip newWaveSound;
    [SerializeField] private AudioClip gameOverSound;
    [SerializeField] private Text resultTime;
    [SerializeField] private Text waveText;
    [SerializeField] private Text timeText;
    [SerializeField] private Text enemyCountText;
    [SerializeField] private List<Wave> waves;
    [SerializeField] private List<GameObject> boosters;
    [SerializeField] private float boosterSpawnCooldown;
    [SerializeField] private Image waveBar;
    [SerializeField] private Image enemySpawnBar;
    [SerializeField] private Image enemySpawnFreezeBar;
    [SerializeField] private Image enemySlowBar;
    [SerializeField] private Image damageUpBar;
    [SerializeField] private GameObject enemyMarker;
    [SerializeField] private GameObject gameOverMenu;
    
    [HideInInspector] public int currentDamage;
    [HideInInspector] public float freezeTime;
    [HideInInspector] public float enemySlowTime;
    [HideInInspector] public float damageUpTime;
    [HideInInspector] public int enemyCount;
    [HideInInspector] public bool gameStopped;
    [HideInInspector] public AudioSource audioSource;
    
    private int _waveCount;
    private int _currentWave;
    private float _currentBoosterCooldown;
    private float _currentTime;
    private float _waveTime;
    private float _enemySpawnTime;
    private float _enemySpawnTimeReducer;
    private Camera _camera;
    private RectTransform _canvas;
    private Animator _gameOverMenuAnimator;

    private void Start()
    {
        freezeTime = 0;
        damageUpTime = 0;
        enemySlowTime = 0;
        
        _waveCount = 0;
        _currentWave = 0;
        _enemySpawnTimeReducer = 0;
        _currentBoosterCooldown = boosterSpawnCooldown;
        _enemySpawnTime = waves[_currentWave].spawnEnemyCooldown - _enemySpawnTimeReducer;

        _camera = Camera.main;
        audioSource = GetComponent<AudioSource>();
        _gameOverMenuAnimator = gameOverMenu.GetComponent<Animator>();
        _canvas = FindObjectOfType<Canvas>().GetComponent<RectTransform>();
        
        NewWave();
    }
    
    private void Update()
    {
        if (gameStopped) return;
        
        _currentTime += Time.deltaTime;
        timeText.text = Math.Round(_currentTime, 2).ToString(CultureInfo.CurrentCulture); 
        
        BarsUpdate();
        enemyCountText.text = enemyCount + "/10";

        if (enemyCount >= 10)
        {
            OnGameOver();
        }
            
        if (_currentBoosterCooldown > 0) _currentBoosterCooldown -= Time.deltaTime;
        else SpawnBooster();

        if (enemySlowTime > 0) enemySlowTime -= Time.deltaTime;
        
        if (damageUpTime > 0)
        {
            damageUpTime -= Time.deltaTime;
            if (currentDamage == 1) currentDamage = 2;
        }
        else if (currentDamage == 2) currentDamage = 1;
        
        if (freezeTime > 0) freezeTime -= Time.deltaTime;
        else
        {
            if (_enemySpawnTime > 0) _enemySpawnTime -= Time.deltaTime;
            else SpawnEnemy();
            
            if (_waveTime > 0) _waveTime -= Time.deltaTime;
            else
            {
                _waveCount++;
                NewWave();
            }
        }
    }

    private void OnGameOver()
    {
        audioSource.PlayOneShot(gameOverSound);
        gameStopped = true;
        resultTime.text = Math.Round(_currentTime, 2).ToString(CultureInfo.CurrentCulture); 
        gameOverMenu.SetActive(true);
        _gameOverMenuAnimator.Play("GameOverScreen");

        var records = new List<float>();
        var recordCount = PlayerPrefs.GetInt("RecordsCount", 0);
        for (var i = 0; i < recordCount; i++)
        {
            records.Add(PlayerPrefs.GetFloat(i + "Records"));
        }
        
        records.Add( (float)Math.Round(_currentTime, 2));
        ReverseSort.Sort(records);

        recordCount++;
        PlayerPrefs.SetInt("RecordsCount", recordCount);
        
        for (var i = 0; i < records.Count; i++)
        {
            if (i < 20) PlayerPrefs.SetFloat(i + "Records", records[i]);
        }
    }

    public void QuitGame()
    {
        audioSource.PlayOneShot(buttonClickSound);
        SceneManager.LoadScene("Menu");
    }
    
    private void BarsUpdate()
    {
        waveBar.fillAmount = _waveTime / (waves[_currentWave].time - _enemySpawnTimeReducer);
        enemySpawnBar.fillAmount = _enemySpawnTime / waves[_currentWave].spawnEnemyCooldown;
        enemySlowBar.fillAmount = enemySlowTime / 3f;
        enemySpawnFreezeBar.fillAmount = freezeTime / 3f;
        damageUpBar.fillAmount = damageUpTime / 5f;
    }
    
    private void NewWave()
    {
        audioSource.PlayOneShot(newWaveSound);
        _currentWave = _waveCount;
        if (_waveCount >= waves.Count)
        {
            _currentWave = waves.Count - 1;
            _enemySpawnTimeReducer += 0.1f;
        }
        
        _waveTime = waves[_currentWave].time;
        waveText.text = "Wave - " + (_waveCount + 1);
    }
    
    private void SpawnBooster()
    {
        audioSource.PlayOneShot(objectSpawnSound);
        _currentBoosterCooldown = boosterSpawnCooldown * Random.Range(5, 15) / 10;
        SpawnObjectOnMap(boosters[Random.Range(0, boosters.Count)]);
    }
    
    // ReSharper disable Unity.PerformanceAnalysis
    private void SpawnEnemy()
    {
        audioSource.PlayOneShot(objectSpawnSound);
        var enemy = SpawnObjectOnMap(waves[_currentWave].enemies[Random.Range(0, waves[_currentWave].enemies.Count)]);
        enemyCount++;
        _enemySpawnTime = (waves[_currentWave].spawnEnemyCooldown - _enemySpawnTimeReducer) * Random.Range(90, 100) / 100;
        
        var enemyPosition = _camera.WorldToScreenPoint(enemy.transform.position);
        if (_camera.pixelRect.Contains(new Vector2(enemyPosition.x, enemyPosition.y))) return;
        var marker = Instantiate(enemyMarker, _canvas);
        
        var ratio = new Vector2();
        if (enemyPosition.x > 0)
        {
            if (enemyPosition.y > 0)
            {
                ratio.x = enemyPosition.x / enemyPosition.y;
                ratio.y = enemyPosition.y / enemyPosition.x;
            }
            else
            {
                ratio.x = enemyPosition.x / Math.Abs(enemyPosition.y);
                ratio.y = -Math.Abs(enemyPosition.y) / enemyPosition.x;
            }
        }
        else
        {
            if (enemyPosition.y > 0)
            {
                ratio.x = -Math.Abs(enemyPosition.x) / enemyPosition.y;
                ratio.y = enemyPosition.y / Math.Abs(enemyPosition.x);
            }
            else
            {
                ratio.x = -Math.Abs(enemyPosition.x) / Math.Abs(enemyPosition.y);
                ratio.y = -Math.Abs(enemyPosition.y) / Math.Abs(enemyPosition.x);
            }
        }
        
        var newCords = new Vector2();
        var rect = _canvas.rect;
        var canvasRadius = new Vector2(rect.width / 2f, rect.height / 2f);
        while (Mathf.Abs(newCords.x) < canvasRadius.x && Mathf.Abs(newCords.y) < canvasRadius.y)
        {
            newCords += ratio;
        }
        
        marker.GetComponent<RectTransform>().localPosition = new Vector3(newCords.x, newCords.y, 0);
        Destroy(marker, 2.0f);
    }

    private static GameObject SpawnObjectOnMap(GameObject obj)
    {
        return Instantiate(obj,
            new Vector3((float)Random.Range(-600, 600) / 100, -6.2f, (float)Random.Range(-200, 1600) / 100),
            Quaternion.identity);
    }
}
