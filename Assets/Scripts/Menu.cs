using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    [SerializeField] private GameObject mainMenu;
    [SerializeField] private GameObject credits;
    [SerializeField] private GameObject records;
    [SerializeField] private Transform listRecords;
    private AudioSource _audioSource;

    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    public void StartGame()
    {
        _audioSource.Play();
        SceneManager.LoadScene("Game");
    }
    
    public void Quit()
    {
        _audioSource.Play();
        Application.Quit();
    }

    public void OpenMainMenu()
    {
        mainMenu.SetActive(true);
        credits.SetActive(false);
        records.SetActive(false);
        _audioSource.Play();
    }
    
    public void OpenCredits()
    {
        mainMenu.SetActive(false);
        credits.SetActive(true);
        _audioSource.Play();
    }
    
    public void OpenRecords()
    {
        mainMenu.SetActive(false);
        records.SetActive(true);
        _audioSource.Play();
        
        for (var i = 0; i < 25; i++)
        {
            if (PlayerPrefs.HasKey(i + "Records"))
            {
                listRecords.GetChild(i).GetChild(0).GetComponent<Text>().text =
                    Math.Round(PlayerPrefs.GetFloat(i + "Records"), 2).ToString(CultureInfo.CurrentCulture);
                continue;
            }

            listRecords.GetChild(i).GetChild(0).GetComponent<Text>().text = "-";
        }
    }
}
