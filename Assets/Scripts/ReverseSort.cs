using System.Collections.Generic;
using UnityEngine;

public static class ReverseSort 
{
    public static void Sort(List<float> list)
    {
        for (var i = list.Count - 1; i > 0; i--)
        {
            for (var j = i - 1; j >= 0; j--)
            {
                if (list[i] > list[j])
                {
                    (list[i], list[j]) = (list[j], list[i]);
                }
            }
        }
    }
}
