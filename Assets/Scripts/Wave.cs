using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Wave : MonoBehaviour
{
    public float time;
    public float spawnEnemyCooldown;
    public List<GameObject> enemies;
}
